﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectiableItems : MonoBehaviour {
	private const float COEFFICIENT_TO_SPAWN_ITEM = 8f;
	[SerializeField] private List<GameObject> collectiableList;

	public bool isCollectiableExist { get; set; }

	private int randItem;
	private GameObject item;

	void Start () {
		item = null;
		isCollectiableExist = false;
	}

	void Update () {
		int index = 1;
		if (Managers.Player.score % COEFFICIENT_TO_SPAWN_ITEM == 0 && Managers.Player.score != 0 && item == null && Game.isGameStarted) {
			index = Random.Range (0, collectiableList.Count);

			float posX = Random.Range (Managers.Screen.leftBound, Managers.Screen.rightBound);
			float posY = Random.Range (2 * Managers.Screen.bottomBound, Managers.Screen.bottomBound);

			item = Instantiate (collectiableList [index], Vector3.zero, collectiableList [index].gameObject.transform.rotation) as GameObject;
			item.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (posX, posY, collectiableList [index].transform.position.z);
		}
	}
}
