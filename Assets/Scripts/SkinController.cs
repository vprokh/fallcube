﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SkinController {
	private const int HIGHSCORE_TO_UNLOCK_SKIN2 = 4;
	private const int HIGHSCORE_TO_UNLOCK_SKIN3 = 14;
	private const int HIGHSCORE_TO_UNLOCK_SKIN4 = 19;
	private const int HIGHSCORE_TO_UNLOCK_SKIN5 = 24;
	private const int HIGHSCORE_TO_UNLOCK_SKIN6 = 29;
	private const int HIGHSCORE_TO_UNLOCK_SKIN7 = 34;
	private const int HIGHSCORE_TO_UNLOCK_SKIN8 = 39;
	private const int HIGHSCORE_TO_UNLOCK_SKIN9 = 49;
	private const string HIGHSCORE_KEY = "HighScore";

	public const int BLOCK_SKIN_COUNT = 3;

	public const string DEFAULT_SKIN = "Just white";
	public const string SKIN2_NAME = "White craters";
	public const string SKIN3_NAME = "Box";
	public const string SKIN4_NAME = "Black craters";
	public const string SKIN5_NAME = "Rubics cube";
	public const string SKIN6_NAME = "Transperent";
	public const string SKIN7_NAME = "Rainbow";
	public const string SKIN8_NAME = "Blue edges";
	public const string SKIN9_NAME = "Just black";

	public const int SKIN_ENABLE = 1;
	public const int SKIN_DISABLE = 0;
	public const string SKIN_RESOURCES_PATH = "Player skins/";
	public const string BLOCK_SKIN_RESOURCES_PATH = "Block skins/";
	public const string CURRENT_SKIN = "currentSkin";

	public static string lastUnlokedSkin;

	public static bool SkinUnlockCheck () {
		lastUnlokedSkin = null;
		int highScore = PlayerPrefs.GetInt (HIGHSCORE_KEY, 0);
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN2 && PlayerPrefs.GetInt (SKIN2_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN2_NAME;
			PlayerPrefs.SetInt (SKIN2_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN3 && PlayerPrefs.GetInt (SKIN3_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN3_NAME;
			PlayerPrefs.SetInt (SKIN3_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN4 && PlayerPrefs.GetInt (SKIN4_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN4_NAME;
			PlayerPrefs.SetInt (SKIN4_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN5 && PlayerPrefs.GetInt (SKIN5_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN5_NAME;
			PlayerPrefs.SetInt (SKIN5_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN6 && PlayerPrefs.GetInt (SKIN6_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN6_NAME;
			PlayerPrefs.SetInt (SKIN6_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN7 && PlayerPrefs.GetInt (SKIN7_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN7_NAME;
			PlayerPrefs.SetInt (SKIN7_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN8 && PlayerPrefs.GetInt (SKIN8_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN8_NAME;
			PlayerPrefs.SetInt (SKIN8_NAME, SKIN_ENABLE);
		}
		if (highScore > HIGHSCORE_TO_UNLOCK_SKIN9 && PlayerPrefs.GetInt (SKIN9_NAME, SKIN_DISABLE) == SKIN_DISABLE) {
			lastUnlokedSkin = SKIN9_NAME;
			PlayerPrefs.SetInt (SKIN9_NAME, SKIN_ENABLE);
		}

		if (lastUnlokedSkin != null)
			return true;

		return false;
	}

	public static int GetHighScoreBySkinName (string skinName) {
		switch (skinName) {
		case SKIN2_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN2;
		case SKIN3_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN3;
		case SKIN4_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN4;
		case SKIN5_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN5;
		case SKIN6_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN6;
		case SKIN7_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN7;
		case SKIN8_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN8;
		case SKIN9_NAME:
			return HIGHSCORE_TO_UNLOCK_SKIN9;
		}

		return 0;
	}
}
