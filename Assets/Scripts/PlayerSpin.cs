﻿using System.Collections;
using UnityEngine;

public class PlayerSpin : MonoBehaviour {
	public float spinSpeed = 50f;
	private Vector3 offset;

	void Update () {
		if (!Game.isGameStarted && !UI.Game.GetFailPopupBackgraund ().gameObject.activeSelf &&
		    !UI.Game.GetPauseBackgraund ().gameObject.activeSelf) {
			transform.Rotate (new Vector3 (Input.GetAxis ("Mouse Y"), Input.GetAxis ("Mouse X"), 0f) * Time.deltaTime * spinSpeed);
		}
	}
	void OnMouseDown () {
		if (!UI.Game.GetPauseBackgraund ().gameObject.activeSelf &&
			!UI.Game.GetFailPopupBackgraund ().gameObject.activeSelf)
			Managers.Player.StopMainAnimation ();

		return;
	}
		
	void OnMouseUp () {
		if (!UI.Game.GetPauseBackgraund ().gameObject.activeSelf &&
			!UI.Game.GetFailPopupBackgraund ().gameObject.activeSelf)
			Managers.Player.StartMainAnimation ();

		return;
	}
}
