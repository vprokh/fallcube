﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (RectTransform))]
public class ChooseSkin : MonoBehaviour {
	private const string DISABLE_SKIN_INFO = "To unlock this skin reach ";
	private const string DISABLE_SKIN_INFO_HIGHSCORE = " high score!";
	private Vector3 NORMAL_SKIN_SCALE = new Vector3 (35f, 35f, 35f);
	[SerializeField] GameObject controllers;

	private Color disabledColor = Color.black;
	private RectTransform rect;
	private Vector3 vectorOffset;
	private Material skinMaterial;
	private GalleryUI galleryUi;

	public float scaleOffset = 0.5f;

	void Start () {
		galleryUi = controllers.GetComponent<GalleryUI> ();
		skinMaterial = GetComponent<Renderer> ().material;
		rect = GetComponent<RectTransform> ();
		vectorOffset = new Vector3 (rect.localScale.x + scaleOffset,
			rect.localScale.y + scaleOffset,
			rect.localScale.z + scaleOffset);

		if (gameObject.name == PlayerPrefs.GetString (SkinController.CURRENT_SKIN, SkinController.DEFAULT_SKIN))
			rect.localScale = vectorOffset;
		SkinActivity ();
	}

	void LateUpdate () {
		if (gameObject.name != Managers.Player.currentSkinName)
			gameObject.transform.localScale = NORMAL_SKIN_SCALE;
	}

	void OnMouseDown () {
		if (skinMaterial.color != disabledColor) {
			Managers.Player.SetSkin (Resources.Load (SkinController.SKIN_RESOURCES_PATH + gameObject.name) as Material);
			PlayerPrefs.SetString (SkinController.CURRENT_SKIN, gameObject.name);
			rect.localScale = vectorOffset;
		} else {
			galleryUi.DisableSkinInfoShow (DISABLE_SKIN_INFO + SkinController.GetHighScoreBySkinName (gameObject.name) + DISABLE_SKIN_INFO_HIGHSCORE);
		}
		return;
	}

	private void SkinActivity () {
		if (PlayerPrefs.GetInt (gameObject.name, 0) == 1 || gameObject.name == SkinController.DEFAULT_SKIN) {
			skinMaterial = Resources.Load (SkinController.SKIN_RESOURCES_PATH + gameObject.name) as Material;
		} else {
			skinMaterial.color = disabledColor;
		}

		return;
	}
}
