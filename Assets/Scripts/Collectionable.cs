﻿using System.Collections;
using UnityEngine;

public class Collectionable : MonoBehaviour {

	private const string GRAB_SPEED_BOOST = "Speed boost";
	private const string GRAB_SPEED_SLOW = "Speed sloow";
	private const string GRAB_WIDTH_LARGE = "Width larger";
	private const string GRAB_IGNORE_BLOCK = "Cube ghost";
	private const string SPEED_BOOST_TAG = "SpeedBoost";
	private const string SPEED_SLOW_TAG = "SpeedSlow";
	private const string WIDTH_LARGE_TAG = "BlockWidth";
	private const string IGNORE_BLOCK_TAG = "OneBlockIgnore";
	private const string PLAYER_TAG = "Player";

	public float durationOfItemTextAnimation = 1f;

	void Update () {
		if (gameObject.transform.position.y >= Managers.Screen.topBound || !Game.isGameStarted)
			Destroy (this.gameObject);
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag != PLAYER_TAG)
			return;
		Color itemColliderColor = GetComponent<Renderer> ().material.color;
		switch (gameObject.tag) {
		case SPEED_SLOW_TAG:
			Messenger.Broadcast<bool> (GameEvent.SPEED_SLOW, true);
			UI.Game.SetGrabText (GRAB_SPEED_SLOW, itemColliderColor);
			break;
		case SPEED_BOOST_TAG:
			Messenger.Broadcast<bool> (GameEvent.SPEED_SLOW, false);
			UI.Game.SetGrabText (GRAB_SPEED_BOOST, itemColliderColor);
			break;
		case WIDTH_LARGE_TAG:
			Messenger.Broadcast<bool> (GameEvent.WIDTH_CHANGE, true);
			UI.Game.SetGrabText (GRAB_WIDTH_LARGE, itemColliderColor);
			break;
		case IGNORE_BLOCK_TAG:
			Managers.Player.SetIgnoreOneBlock (true);
			UI.Game.SetGrabText (GRAB_IGNORE_BLOCK, itemColliderColor);
			break;
		default:
			break;
		}

		Destroy (this.gameObject);

		return;
	}
}
