﻿using UnityEngine;

public class PlayerCloneDestroy : MonoBehaviour {
	void Update () {
		if (gameObject.transform.position.y <= Managers.Screen.bottomBound) {
			Destroy (this.gameObject);
		}
	}

	void OnColliderEnter (Collision other) {
		Managers.Audio.PlayCubesColliderSound ();
	}
}
