﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using System.IO;

public class FailPopup : MonoBehaviour {
	private const string SCENE_NAME = "main";
	private const string SCREENSHOT_NAME = "fallCube_screenshot.png";
	private const string ANDROID_INTENT = "android.content.Intent";
	private const string ANDROID_URI = "android.net.Uri";
	private const int SCREENSHOT_SUPER_SIZE = 2;
	private const string PLACEMENT_ID = "rewardedVideo";
	private const float SCREEN_CAPTURE_DELAY = 0.3f;
	private const string SCREENSHOT_TYPE = "image/jpeg";
	private const string SHARE_MESSAGE = "This is my best score in game Fall Cube. Can you beat it?";
	private const string SHARE_TITLE = "Score share";
	private const string UNITY_PLAYER_CLASS = "com.unity3d.player.UnityPlayer";


	[SerializeField] private Button oneMoreChance;
	[SerializeField] private AudioClip buttonsClick;

	private bool isShareProcessing;

	void Start () {
		isShareProcessing = false;
	}

	void Update () {
		if (oneMoreChance.interactable && !GameEvent.isRewarding)
			oneMoreChance.interactable = Advertisement.IsReady (PLACEMENT_ID);
		else if (oneMoreChance.interactable && GameEvent.isRewarding) {
			oneMoreChance.interactable = false;
			GameEvent.isRewarding = false;
		}
	}

	public void OnRestart () {
		if (GameEvent.isSoundOn)
		Managers.Audio.PlaySource (buttonsClick);
		Messenger.Broadcast (GameEvent.RESTART_GAME);
		if (!oneMoreChance.interactable)
			oneMoreChance.interactable = true;

		return;
	}

	public void OnToHome () {
		Managers.Audio.PlaySource (buttonsClick);
		Messenger.Cleanup ();
		SceneManager.LoadScene (SCENE_NAME);
		return;
	}

	public void OnShare () {
		Managers.Audio.PlaySource (buttonsClick);
		Debug.Log ("Share");
		if (!isShareProcessing) {
			StartCoroutine (ShareScreenshot ());
		}

		return;
	}

	public void OnOneMoreChance () {
		Managers.Audio.PlaySource (buttonsClick);
		ShowRewardedVideo ();

		return;
	}

	private IEnumerator ShareScreenshot () {
		isShareProcessing = true;
		//yield return new WaitForSeconds(0.5f);
		ScreenCapture.CaptureScreenshot (SCREENSHOT_NAME, SCREENSHOT_SUPER_SIZE);
		string path = Path.Combine (Application.persistentDataPath, SCREENSHOT_NAME);

		yield return new WaitForSecondsRealtime (SCREEN_CAPTURE_DELAY);
		if (!Application.isEditor) {
			AndroidJavaClass intentClass = new AndroidJavaClass (ANDROID_INTENT);
			AndroidJavaObject intentObject = new AndroidJavaObject (ANDROID_INTENT);
			intentObject.Call<AndroidJavaObject> ("setAction", intentClass.GetStatic<string> ("ACTION_SEND"));
			AndroidJavaClass uriClass = new AndroidJavaClass (ANDROID_URI);
			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject> ("parse", "file://" + path);
			intentObject.Call<AndroidJavaObject> ("putExtra", intentClass.GetStatic<string> ("EXTRA_STREAM"),
				uriObject);
			intentObject.Call<AndroidJavaObject> ("putExtra", intentClass.GetStatic<string> ("EXTRA_TEXT"),
				SHARE_MESSAGE);
			intentObject.Call<AndroidJavaObject> ("setType", SCREENSHOT_TYPE);
			AndroidJavaClass unity = new AndroidJavaClass (UNITY_PLAYER_CLASS);
			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
			AndroidJavaObject chooseApp = intentClass.CallStatic<AndroidJavaObject> ("createChooser", intentObject, SHARE_TITLE);

			currentActivity.Call ("startActivity", chooseApp);
			yield return new WaitForSecondsRealtime (SCREEN_CAPTURE_DELAY * 3f);
		} else
			Debug.Log ("You are in editor!!!");
		isShareProcessing = false;
	}

	private void ShowRewardedVideo () {
		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;

		Advertisement.Show(PLACEMENT_ID, options);
	}

	private void HandleShowResult (ShowResult result) {
		if(result == ShowResult.Finished) {
			Debug.Log("Video completed - Offer a reward to the player");
			GameEvent.isRewarding = true;
			OnRestart ();
		} else if(result == ShowResult.Skipped) {
			Debug.LogWarning("Video was skipped - Do NOT reward the player");

		} else if(result == ShowResult.Failed) {
			Debug.LogError("Video failed to show");
		}
	}

}
