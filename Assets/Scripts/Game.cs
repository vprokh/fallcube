﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class Game : MonoBehaviour {
	private const string GAME_ID = "1724325";
	private const int LOSE_COUNTER_TO_ADS_SHOW = 5;
	private const string GAME_NAME = "Fall cube";
	private const string SETTINGS_TITLE = "Settings";
	private const string GALLERY_TITLE = "Gallery";
	private const string HIGHSCORE_KEY = "HighScore";
	private const int DIMON_NUMBER = 3;

	[SerializeField] private GameObject controllers;
	[SerializeField] private AudioClip buttonsClick;
	[SerializeField] private AudioClip failSound;
	[SerializeField] private AudioClip scoreCollect;
	[SerializeField] private AudioClip dimonAudio;

	public static bool isGameStarted;

	void Awake () {
		if (Advertisement.isSupported)
			Advertisement.Initialize (GAME_ID, true);
		Messenger.AddListener (GameEvent.FAIL_MOVE, OnFailMove);
		Messenger.AddListener (GameEvent.SUCCESSFUL_MOVE, OnSuccessfulMove);
		Messenger.AddListener (GameEvent.RESTART_GAME, OnRestartGame);
	}

	void Destroy () {
		Messenger.RemoveListener (GameEvent.FAIL_MOVE, OnFailMove);
		Messenger.RemoveListener (GameEvent.SUCCESSFUL_MOVE, OnSuccessfulMove);
		Messenger.RemoveListener (GameEvent.RESTART_GAME, OnRestartGame);
	}

	void Start () {
		isGameStarted = false;
		GameEvent.isRewarding = false;
	}

	public void FirstGameStart () {
		UI.Game.ScrollUp ();
		UI.Game.PlayButtonShow (false);
		OnStartGame ();
		return;
	}

	public void OnStartGame () {
		Messenger.Broadcast (GameEvent.SPAWN_BLOCK);
		Managers.Player.AnimatePlayer ();
		isGameStarted = true;
		if (!GameEvent.isRewarding)
			Managers.Player.UpdateData ();

		UI.Game.ShowCurrentScore ();

		return;
	}

	private void OnRestartGame () {
		Managers.Player.NoFreezeAfterFail ();
		Managers.Player.SetIgnoreOneBlock (false);
		UI.Game.FailPopupShow (false);
		SpawnBlocks.DestroyAllBlocks ();
		if (GameEvent.isMusicOn)
			Managers.Audio.PlayBackgroundMusic ();
		controllers.GetComponent<SpawnBlocks> ().UpdateValues ();

		OnStartGame ();

		return;
	}

	private void OnFailMove () {
		GameEvent.loseCounter++;
		int highScore = PlayerPrefs.GetInt (HIGHSCORE_KEY, 0);
		if (GameEvent.isMusicOn)
			Managers.Audio.StopMusic ();
		
		Managers.Audio.PlaySource (failSound);	
		isGameStarted = false;
		UI.Game.ShowFinalScores ();

		if (GameEvent.loseCounter == LOSE_COUNTER_TO_ADS_SHOW && Advertisement.IsReady ()) {
			GameEvent.loseCounter = 0;
			Advertisement.Show ();
		} else
			Debug.LogWarning ("Ads not ready!");

		return;
	}

	private void OnSuccessfulMove () {
		Managers.Audio.PlaySource (scoreCollect);
		Managers.Player.ScoreIncreaze ();
		UI.Game.ShowCurrentScore ();

		return;
	}

	public void OnSettingsOpen (bool isOpening) {
		Managers.Audio.PlaySource (buttonsClick);
		UI.Game.PlayButtonShow (!isOpening);
		UI.Game.MainButtonsShow (!isOpening);
		UI.Settings.ShowSettingsMenu (isOpening);
		if (isOpening)
			UI.Game.SetGameName (SETTINGS_TITLE);
		else
			UI.Game.SetGameName (GAME_NAME);

		return;
	}

	public void OnLeadersOpen () {
		Managers.Audio.PlaySource (buttonsClick);
		Debug.Log ("Leaders");
		return;
	}

	public void OnGalleryOpen (bool isOpening) {
		Managers.Audio.PlaySource (buttonsClick);
		UI.Game.PlayButtonShow (!isOpening);
		UI.Game.MainButtonsShow (!isOpening);
		UI.Gallery.ShowGalleryMenu (isOpening);
		if (isOpening)
			UI.Game.SetGameName (GALLERY_TITLE);
		else
			UI.Game.SetGameName (GAME_NAME);
		
		return;
	}
}
