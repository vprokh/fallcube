﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
	private const string PLAYER_TAG = "Player";
	private const string WINNING_BLOCK_TAG = "WinningBlock";
	private const float BLOCK_EXIT_DELAY = 0.5f;

	private float topScreenBound;
	private bool isScored;

	void Start () {
		topScreenBound = Managers.Screen.topBound;
		isScored = false;
	}

	void Update () {
		if (0 <= transform.position.y && !isScored) {
			string blockTag = gameObject.tag;
			if (blockTag == WINNING_BLOCK_TAG) {
				Messenger.Broadcast (GameEvent.SUCCESSFUL_MOVE);
				Messenger.Broadcast (GameEvent.SPAWN_BLOCK);
			}
			isScored = true;
		}
		if (transform.position.y >= topScreenBound) {
			Destroy (gameObject);
			isScored = false;
		}
	}

	void OnCollisionEnter (Collision other) {
		if (!Managers.Player.isIgnoreBlock && Game.isGameStarted)
			Messenger.Broadcast (GameEvent.FAIL_MOVE);
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject.tag == PLAYER_TAG) {
			StartCoroutine (WaitForBlockExit ());
			Debug.Log ("Ghost cobe false");
		}
		return;
	}

	private IEnumerator WaitForBlockExit () {
		yield return new WaitForSeconds (BLOCK_EXIT_DELAY);
		Managers.Player.SetIgnoreOneBlock (false);
	}
}
