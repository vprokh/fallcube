﻿using UnityEngine;
using System.Collections;

public class ScrollBackground : MonoBehaviour {
	public static float speed;
	public float tileSizeY;

	private Vector3 startPosition;

	void Start () {
		speed = 1f;
		startPosition = transform.position;
	}

	void Update () {
		if (Game.isGameStarted) {
			float newPosition = Mathf.Repeat (Time.time * speed, tileSizeY);
			transform.position = startPosition + Vector3.up * newPosition;
		}
	}
}