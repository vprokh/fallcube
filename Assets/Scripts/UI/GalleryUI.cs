﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryUI : MonoBehaviour {
	private const float GALLERY_INFO_SHOW_DELAY = 2f;

	[SerializeField] private GameObject galleryMenu;
	[SerializeField] private List<GameObject> skinsList;
	[SerializeField] private Image leftArrow;
	[SerializeField] private Image rightArrow;
	[SerializeField] private Text disableSkinInfo;

	public float scrollSpeed = 6f;
	public float infoTextFadeSpeed = 2f;

	private bool isScrollLeftEnable;
	private bool isScrollRightEnable;
	private int currentSkinList;

	void Start () {
		galleryMenu.gameObject.SetActive (false);
		currentSkinList = 0;
		isScrollLeftEnable = false;
		isScrollRightEnable = false;
	}

	void LateUpdate () {
		if (galleryMenu == null)
			return;
		if (currentSkinList == 0 && leftArrow.GetComponent<Button> ().IsInteractable ()) {
			leftArrow.GetComponent<Button> ().interactable = false;
			rightArrow.GetComponent<Button> ().interactable = true;
		} else if (currentSkinList == skinsList.Count - 1 && rightArrow.GetComponent<Button> ().IsInteractable ()) {
			leftArrow.GetComponent<Button> ().interactable = true;
			rightArrow.GetComponent<Button> ().interactable = false;
		}

		if (isScrollLeftEnable && skinsList[currentSkinList].GetComponent <RectTransform> ().localPosition.x >= Managers.Screen.leftBound) {
			skinsList [currentSkinList].GetComponent <RectTransform> ().localPosition -= new Vector3 (scrollSpeed, 0, 0);
			skinsList [currentSkinList - 1].GetComponent <RectTransform> ().localPosition -= new Vector3 (scrollSpeed, 0, 0);

		}
		if (isScrollRightEnable && skinsList[currentSkinList].GetComponent <RectTransform> ().localPosition.x <= Managers.Screen.rightBound) {
			skinsList [currentSkinList].GetComponent <RectTransform> ().localPosition += new Vector3 (scrollSpeed, 0, 0);
			skinsList [currentSkinList + 1].GetComponent <RectTransform> ().localPosition += new Vector3 (scrollSpeed, 0, 0);
		}

		if (Game.isGameStarted)
			Destroy (galleryMenu.gameObject);

	}

	public void ShowGalleryMenu (bool isShow) {
		galleryMenu.gameObject.SetActive (isShow);

		return;
	}

	public void OnLeftChange () {
		currentSkinList--;
		skinsList [currentSkinList].gameObject.SetActive (true);
		isScrollRightEnable = true;
		isScrollLeftEnable = false;

		return;
	}

	public void OnRightChange () {
		currentSkinList++;
		skinsList [currentSkinList].gameObject.SetActive (true);
		isScrollLeftEnable = true;
		isScrollRightEnable = false;

		return;
	}

	public void DisableSkinInfoShow (string infoToShow) {
		disableSkinInfo.text = infoToShow;
		StopAllCoroutines ();
		StartCoroutine (ShowInfo ());
	}

	private IEnumerator ShowInfo () {
		disableSkinInfo.color = new Color (disableSkinInfo.color.r, disableSkinInfo.color.g, disableSkinInfo.color.b, 1f);

		disableSkinInfo.gameObject.SetActive (true);

		yield return new WaitForSeconds (GALLERY_INFO_SHOW_DELAY);

		disableSkinInfo.gameObject.SetActive (false);
	}
}