﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsUI : MonoBehaviour {
	private const string MAIN_SCENE_NAME = "main";

	[SerializeField] private GameObject managers;
	[SerializeField] private GameObject settingsMenu;
	[SerializeField] private Slider soundSlider;
	[SerializeField] private Slider musicSlider;
	[SerializeField] private RawImage resetWindowBackgraund;
	[SerializeField] private Button resetButton;
	[SerializeField] private AudioClip buttonsClick;
	[SerializeField] private Button backButton;

	private Color enabledColor;
	private Color disabledColor;

	void Start () {
		settingsMenu.gameObject.SetActive (false);

		enabledColor = new Color (0.16f, 1f, 1f);
		disabledColor = new Color (0.8f, 0.8f, 0.8f);

		if (!GameEvent.isMusicOn) {
			OnMusic (musicSlider.minValue);
		}
		if (!GameEvent.isSoundOn) {
			OnSound (soundSlider.minValue);
		}
	}

	public void ShowSettingsMenu (bool isShow) {
		settingsMenu.gameObject.SetActive (isShow); 

		return;
	}

	public void OnSound (float value) {
		GameEvent.isSoundOn = value == soundSlider.maxValue;

		if (value == soundSlider.maxValue) {
			ChangeSoundColors (enabledColor);
		} else {
			ChangeSoundColors (disabledColor);
		}
		Managers.Audio.soundOn = !GameEvent.isSoundOn;
		soundSlider.value = value;

		return;
	}

	public void OnMusic (float value) {
		GameEvent.isMusicOn = value == musicSlider.maxValue;

		if (value == musicSlider.maxValue) {
			Managers.Audio.PlayBackgroundMusic ();
			ChangeMusicColors (enabledColor);
		} else {
			Managers.Audio.StopMusic ();
			ChangeMusicColors (disabledColor);
		}
		musicSlider.value = value;

		return;
	}

	private void ChangeSoundColors (Color color) {
		ColorBlock soundColorBlock = soundSlider.colors;
		soundColorBlock.normalColor = color;
		soundColorBlock.highlightedColor = color;
		soundColorBlock.pressedColor = color;
		soundSlider.colors = soundColorBlock;

		return;
	}

	private void ChangeMusicColors (Color color) {
		ColorBlock musicColorBlock = musicSlider.colors;
		musicColorBlock.normalColor = color;
		musicColorBlock.highlightedColor = color;
		musicColorBlock.pressedColor = color;
		musicSlider.colors = musicColorBlock;

		return;
	}

	private void SettingsElementVisibility (bool isVisible) {
		musicSlider.gameObject.SetActive (isVisible);
		soundSlider.gameObject.SetActive (isVisible);
		resetButton.gameObject.SetActive (isVisible);
		Managers.Player.SetVisibility (isVisible);
		backButton.gameObject.SetActive (isVisible);
		resetWindowBackgraund.gameObject.SetActive (!isVisible);

		return;
	}

	public void OnResetHihgScore () {
		Managers.Audio.PlaySource (buttonsClick);
		SettingsElementVisibility (false);

		return;
	}

	public void OnResetHighScore (bool isAnswerYes) {
		if (!isAnswerYes) {
			SettingsElementVisibility (true);
		} else {
			PlayerPrefs.DeleteAll ();
			Messenger.Cleanup ();
			SceneManager.LoadScene (MAIN_SCENE_NAME);
		}

		return;
	}
}
