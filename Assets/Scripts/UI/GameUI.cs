﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameUI : MonoBehaviour {
	private const string NEW_SKIN_NAME = "New skin unlocked:\n";
	private const string BEST_SCORE_KEY = "HighScore";
	private const int SPAWN_CUBE_COUNT = 30;
	private const float MAIN_BUTTON_SPEED = 10f;
	private const float BEST_SCORE_ANIMATION_DELAY = 1f;
	private const float MAIN_BUTTON_DESTROY_DELAY = 2f;

	[SerializeField] private Button playButton;
	[SerializeField] private List<Image> buttonsList;
	[SerializeField] private Text gameName;
	[SerializeField] private Image failMenu;
	[SerializeField] private Image pauseButton;
	[SerializeField] private Image pauseBackground;
	[SerializeField] private Text highScore;
	[SerializeField] private Text currentScore;
	[SerializeField] private AudioClip eeeBoy;
	[SerializeField] private Text grabItemText;
	[SerializeField] private Button newSkinOkButton;
	[SerializeField] private Button newSkinEquipButton;
	[SerializeField] private GameObject playerClonePrefab;
	[SerializeField] private AudioClip buttonsClick;
	[SerializeField] private Text newSkinName;

	private Color newBestColor;
	private float timeBetweenCubeSpawn = 0.1f;
	public bool isSpawnEnabled { get; private set; }

	void Start () {
		newSkinName.gameObject.SetActive (false);
		newSkinEquipButton.gameObject.SetActive (false);
		newSkinOkButton.gameObject.SetActive (false);
		playButton.gameObject.SetActive (true);
		gameName.gameObject.SetActive (true);
		failMenu.gameObject.SetActive (false);
		pauseButton.gameObject.SetActive (false);
		pauseBackground.gameObject.SetActive (false);
		highScore.gameObject.SetActive (false);
		currentScore.gameObject.SetActive (false);

		Managers.Player.SetSkin (Resources.Load (SkinController.SKIN_RESOURCES_PATH + PlayerPrefs.GetString (SkinController.CURRENT_SKIN, SkinController.DEFAULT_SKIN)) as Material);
		newBestColor = new Color (0f, 0.78f, 0f);
	}

	void Update () {
	}

	public void PlayButtonShow (bool isActive) {
		playButton.gameObject.SetActive (isActive);

		return;
	}

	public void ShowCurrentScore () {
		int currentScore = Managers.Player.score;
		if (currentScore > PlayerPrefs.GetInt (BEST_SCORE_KEY, 0)) {
			gameName.color = newBestColor;
			gameName.GetComponent<Animation> ().Play ();
		} else 
			gameName.color = Color.white;

		gameName.text = currentScore.ToString (); 
		return;
	}

	public void FailPopupShow (bool active) {
		failMenu.gameObject.SetActive (active);
		gameName.gameObject.SetActive (!active);
		pauseButton.gameObject.SetActive (!active);

		return;
	}

	public void SetGameName (string name) {
		gameName.text = name;

		return;
	}
		
	public void OnPaused (bool active) {
		if (active && GameEvent.isMusicOn)
			Managers.Audio.StopMusic ();
		else if (!active && GameEvent.isMusicOn)
			Managers.Audio.PlayBackgroundMusic ();
		Game.isGameStarted = !active;
		pauseButton.gameObject.SetActive (!active);
		pauseBackground.gameObject.SetActive (active);

		return;
	}

	private IEnumerator SpawnCube() {
		int cubeCount = 0;
		GameObject cubeClone;
		Vector3 cubePosition;
		Quaternion cubeQuaternion;
		Color cubeColor;
		Managers.Player.NoFreezeAfterFail ();

		while (cubeCount != SPAWN_CUBE_COUNT) {
			cubeCount++;
			cubePosition = new Vector3 (Random.Range (Managers.Screen.leftBound, Managers.Screen.rightBound),
				Managers.Screen.topBound,
				playerClonePrefab.transform.position.z);
			cubeQuaternion = Quaternion.Euler (new Vector3 (Random.Range (0f, 360f),
				Random.Range (0f, 360f), Random.Range (0f, 360f)));
			cubeColor = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

			yield return new WaitForSeconds (timeBetweenCubeSpawn);
			cubeClone = Instantiate (playerClonePrefab, cubePosition, cubeQuaternion);
			cubeClone.GetComponent<Renderer> ().material.color = cubeColor;
			
		}

		Managers.Player.SetSkin (Resources.Load(SkinController.SKIN_RESOURCES_PATH + SkinController.lastUnlokedSkin) as Material);
		Managers.Player.StartNewSkinAnimation ();
		newSkinName.text = NEW_SKIN_NAME + SkinController.lastUnlokedSkin;

		newSkinOkButton.gameObject.SetActive (true);
		newSkinEquipButton.gameObject.SetActive (true);
		newSkinName.gameObject.SetActive (true);
	}

	public void ShowFinalScores () {
		int best = PlayerPrefs.GetInt (BEST_SCORE_KEY, 0);
		int score = Managers.Player.score; 

		highScore.transform.position = new Vector3 (highScore.transform.position.x,
			Managers.Screen.topBound + ScreenManager.SCREEN_OFFSET,
			highScore.transform.position.z);
		currentScore.transform.position = new Vector3 (currentScore.transform.position.x,
			Managers.Screen.topBound + ScreenManager.SCREEN_OFFSET,
			currentScore.transform.position.z);
		currentScore.gameObject.SetActive (true);
		highScore.gameObject.SetActive (true);

		StartCoroutine (ScoreAnimation ());
		FailPopupShow (true);

		currentScore.text = "Your Score:\n" + score;
		if (best == 0 && score == 0) {
			highScore.color = new Color (0f, 0f, 0f);
			highScore.text = "Try again :)";
		} else if (score > best) {
			highScore.color = new Color (0f, 0.9f, 0f);
			highScore.text = "New Best !!!";
			PlayerPrefs.SetInt (BEST_SCORE_KEY, score);
			Managers.Audio.PlaySource (eeeBoy);
			if (SkinController.SkinUnlockCheck ()) {
				ShowNewSkin (true);
			}
		} else {
			highScore.color = new Color (0f, 0f, 0f);
			highScore.text = "Best:\n" + best.ToString ();
		}

		return;
	}

	private void ShowNewSkin (bool isShow) {
		Managers.Player.SetVisibility (!isShow);
		failMenu.gameObject.SetActive (!isShow);

		if (isShow) {
			SpawnBlocks.DestroyAllBlocks ();
			StartCoroutine (SpawnCube ());
		}

		return;
	}

	public void OnNewSkinButtons (bool isEquip) {
		Managers.Audio.PlaySource (buttonsClick);
		if (!isEquip) {
			Managers.Player.SetSkin (Resources.Load (SkinController.SKIN_RESOURCES_PATH + PlayerPrefs.GetString (SkinController.CURRENT_SKIN)) as Material);
		} else {
			PlayerPrefs.SetString (SkinController.CURRENT_SKIN, SkinController.lastUnlokedSkin);
		}

		newSkinOkButton.gameObject.SetActive (false);
		newSkinEquipButton.gameObject.SetActive (false);
		newSkinName.gameObject.SetActive (false);

		ShowNewSkin (false);

		return;
	}

	private IEnumerator MainButtonsDestroy () {
		yield return new WaitForSeconds (MAIN_BUTTON_DESTROY_DELAY);
		foreach (Image button in buttonsList) {
			Destroy (button.gameObject);
		}
	}

	public void MainButtonsShow (bool isActive) {
		foreach (Image button in buttonsList) {
			button.gameObject.SetActive (isActive);
		}

		return;
	}

	public void ScrollUp () {
		pauseButton.gameObject.SetActive (true);
		ScrollObjects scrollObject;
		foreach (Image button in buttonsList) {
			scrollObject = button.GetComponent <ScrollObjects> ();
			scrollObject.speed = MAIN_BUTTON_SPEED;
			scrollObject.checkPosition = Managers.Screen.topBound * 125f;
		}
		StartCoroutine (MainButtonsDestroy ());

		return;
	}

	private IEnumerator ScoreAnimation () {
		yield return new WaitForSeconds (BEST_SCORE_ANIMATION_DELAY);
		highScore.GetComponent <Animation> ().Play ();
	}



	public void SetGrabText (string text, Color itemColor) {
		grabItemText.text = text;
		grabItemText.color = itemColor;
		grabItemText.gameObject.SetActive (true);
		grabItemText.GetComponent<Animation> ().Play ();

		return;
	}

	public Image GetPauseBackgraund () {
		return pauseBackground;
	}

	public Image GetFailPopupBackgraund () {
		return failMenu;
	}
}
