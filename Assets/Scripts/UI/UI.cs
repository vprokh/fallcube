﻿using UnityEngine;

[RequireComponent (typeof (GameUI))]
[RequireComponent (typeof (SettingsUI))]
[RequireComponent (typeof (GalleryUI))]
public class UI : MonoBehaviour {
	public static GameUI Game;
	public static SettingsUI Settings;
	public static GalleryUI Gallery;

	void Awake () {
		Game = GetComponent<GameUI> ();
		Settings = GetComponent<SettingsUI> ();
		Gallery = GetComponent<GalleryUI> ();
	}
}
