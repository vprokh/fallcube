﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlocks : MonoBehaviour {
	private const float MIN_BLOCK_SCALE = 0.5f;
	private const float MAX_BLOCKS_WIDTH = 2f;
	private const float SPEED_STEP = 0.005f;
	private const float WIDTH_STEP = 0.1f;
	private const float WIDTH_LARGE_COEFFICIENT = 10f;
	private const float SPEED_BOOST_COEFFICIENT = 7f;
	private const float BOTTOM_SCREEN_OFFSET = 2f;
	private const float WIDTH_LARGE_TIME = 7f;
	private const string WINNING_BLOCK_TAG = "WinningBlock";

	[SerializeField] private GameObject blockPrefab;

	private static List<GameObject> blockList;
	private float screenSize;
	private float bottomBound;
	private float currentSpeed;
	private float widthOffset = MAX_BLOCKS_WIDTH / 2;
	private int currentBlockSkinIndex;

	void Awake () {
		Messenger.AddListener<bool> (GameEvent.WIDTH_CHANGE, OnWidthChange);
		Messenger.AddListener<bool> (GameEvent.SPEED_SLOW, OnSpeedChange);
		Messenger.AddListener (GameEvent.SPAWN_BLOCK, OnSpawn);
	}

	void Destroy () {
		Messenger.RemoveListener<bool> (GameEvent.WIDTH_CHANGE, OnWidthChange);
		Messenger.RemoveListener<bool> (GameEvent.SPEED_SLOW, OnSpeedChange);
		Messenger.RemoveListener (GameEvent.SPAWN_BLOCK, OnSpawn);
	}

	void Start () {
		currentSpeed = 0f;
		screenSize = Managers.Screen.screenSize;
		bottomBound = Managers.Screen.bottomBound - BOTTOM_SCREEN_OFFSET;
		blockList = new List<GameObject> ();
		currentBlockSkinIndex = Random.Range (0, SkinController.BLOCK_SKIN_COUNT);
	}

	public void UpdateValues () {
		if (GameEvent.isRewarding) {
			currentSpeed /= 2f;
		} else {
			currentSpeed = 0f;
			widthOffset = MAX_BLOCKS_WIDTH / 2f;
		}

		return;
	}

	private void OnSpawn () {
		bool isSecond = false;
		DeleteOldBlocks ();
		HarderLevel ();
		float blockScaleX = CreateBlock (screenSize, isSecond, 0f);

		if (blockScaleX < screenSize - MIN_BLOCK_SCALE) {
			isSecond = true;
			CreateBlock (screenSize, isSecond, blockScaleX);
		}

		return;
	}

	private float CreateBlock (float maxBlockScale, bool isSecond , float localScaleXFirstBlock) {
		Vector3 blockScale = Vector3.zero;
		if (isSecond) {
			blockScale = new Vector3 (maxBlockScale - localScaleXFirstBlock - widthOffset,
			blockPrefab.transform.localScale.y, blockPrefab.transform.localScale.z);
			if (blockScale.x <= 0) {
				return 0f;
			}
		} else {
			blockScale = new Vector3 (Random.Range (MIN_BLOCK_SCALE, maxBlockScale),
			blockPrefab.transform.localScale.y, blockPrefab.transform.localScale.z);
		}

		float blockPositionX = Managers.Screen.screenSize / 2f - blockScale.x / 2f + Managers.Player.startXScale / 2f;
		if (isSecond) 
			blockPositionX *= -1;
		
		Vector3 blockPosition = new Vector3 (blockPositionX, bottomBound, blockPrefab.transform.position.z);
		GameObject block = Instantiate (blockPrefab, blockPosition, Quaternion.identity) as GameObject;
		block.GetComponent<ScrollObjects> ().speed += currentSpeed;

		if (!isSecond)
			block.gameObject.tag = WINNING_BLOCK_TAG;
		
		block.transform.localScale = blockScale;
		block.transform.position = blockPosition;
		block.GetComponent<Renderer> ().material = Resources.Load (SkinController.BLOCK_SKIN_RESOURCES_PATH + currentBlockSkinIndex) as Material;
		blockList.Add (block);

		return block.transform.localScale.x;
	}

	private void DeleteOldBlocks () {
		for (int i = 0; i < blockList.Count; ++ i) {
			if (blockList [i] == null) {
				blockList.RemoveAt (i);
			}
		}

		return;
	}

	public static List<GameObject> GetBlockList () {
		foreach (GameObject block in blockList) {
		}
		return blockList;
	}

	public static void DestroyAllBlocks () {
		for (int i = 0; i < blockList.Count; ++i) {
			Destroy (blockList [i].gameObject);
		}
		blockList.Clear ();
	}

	private void HarderLevel () {
		int score = Managers.Player.score;
		if (score % SPEED_BOOST_COEFFICIENT == 0 && score != 0) {
			currentSpeed += SPEED_STEP;
		}
		if (score % WIDTH_LARGE_COEFFICIENT == 0 && score != 0) {
			widthOffset -= WIDTH_STEP;
			if (widthOffset < 0)
				widthOffset = 0;
		}

		return;
	}

	private void OnWidthChange (bool isWidther) {
		if (isWidther) {
			StopAllCoroutines ();
			StartCoroutine (WidthChangeTime ());
		}
		else
			//TODO SMALLER width
		
		return;
	}

	private IEnumerator WidthChangeTime () {
		float startWidthOffset = widthOffset;
		widthOffset += WIDTH_STEP * 2f;
		if (widthOffset > MAX_BLOCKS_WIDTH)
			widthOffset = MAX_BLOCKS_WIDTH;
		yield return new WaitForSeconds (WIDTH_LARGE_TIME);
		widthOffset = startWidthOffset;
	}

	private void OnSpeedChange (bool isSpeedSlowing) {
		if (isSpeedSlowing) {
			currentSpeed -= SPEED_STEP;
		} else {
			currentSpeed += SPEED_STEP;
		}

		return;
	}
}
