﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof (RectTransform))]
public class ScrollObjects : MonoBehaviour {
	public float speed = 2f;
	public bool notGamingObject;
	public float checkPosition;
	private RectTransform rect;

	void Start () {	
		rect = GetComponent <RectTransform> ();
	}

	void LateUpdate () {
		if (rect.offsetMin.y != checkPosition && (Game.isGameStarted || notGamingObject)) {
			Scroll ();
		}
	}

	public void Scroll () {
		rect.offsetMax += new Vector2 (0, speed);
		rect.offsetMin += new Vector2 (0, speed);
	
		return;
	}
}
