﻿using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	[SerializeField] private AudioSource soundSource;
	[SerializeField] private AudioSource musicSource;
	[SerializeField] private string backgroundMusic;
	[SerializeField] private string fallingMusic;
	[SerializeField] private AudioClip cubesColliderSound;

	private const string MUSIC_RESOURCE_PATH = "Music/";

	public bool soundOn {
		get { return soundSource.mute; }
		set { soundSource.mute = value; }
	}

	public void PlayBackgroundMusic () {
		PlayMusic (Resources.Load (MUSIC_RESOURCE_PATH + backgroundMusic) as AudioClip);
			
		return;
	}

	public void PlayFallingMusic () {
		PlayMusic (Resources.Load (MUSIC_RESOURCE_PATH + fallingMusic) as AudioClip);

		return;
	}

	private void PlayMusic (AudioClip clip) {
		musicSource.clip = clip;
		musicSource.Play ();

		return;
	}

	public void PlaySource (AudioClip clip) {
		soundSource.PlayOneShot (clip);

		return;
	}

	public void StopMusic () {
		musicSource.Stop ();
		return;
	}

	public void PlayCubesColliderSound () {
		soundSource.PlayOneShot (cubesColliderSound);
		return;
	}
}
