﻿using UnityEngine;

[RequireComponent (typeof (ScreenManager))]
[RequireComponent (typeof (AudioManager))]
[RequireComponent (typeof (PlayerManager))]

public class Managers : MonoBehaviour {
	public static ScreenManager Screen;
	public static AudioManager Audio;
	public static PlayerManager Player;

	void Awake () {
		Player = GetComponent <PlayerManager> ();
		Screen = GetComponent <ScreenManager> ();
		Audio = GetComponent <AudioManager> ();
	}
}
