﻿using System.Collections;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
	public const string PLAYER_ROTATION_ANIMATION = "playerRotationAnimation";
	public const string PLAYER_GAME_POSITION = "playerPositionOnStartGame";
	public const string NEW_SKIN_ANIMATION = "newSkinPlayer";

	[SerializeField] private GameObject player;

	public float speed = 5f;
	public bool isIgnoreBlock { get; private set; }
	public int score { get; private set; }
	public float startXScale { get; private set; }
	public string currentSkinName { get; private set; }

	private Rigidbody playerRigitBody;


	void Start () {
		score = 0;
		isIgnoreBlock = false;
		startXScale = player.gameObject.transform.localScale.x;
		playerRigitBody = player.GetComponent <Rigidbody> ();

		return;
	}

	void Update () {
		if (Game.isGameStarted)
			PlayerMove ();

		return;
	}

	public void SetVisibility (bool isVisible) {
		player.gameObject.SetActive (isVisible);
		return;
	}

	public void AnimatePlayer () {
		StopMainAnimation ();
		StartTrueScaleAnimation ();

		return;
	}
		
	public void UpdateData () {
		score = 0;
		playerRigitBody.freezeRotation = false;

		return;
	}

	public void NoFreezeAfterFail () {
		playerRigitBody.velocity = Vector3.zero;
		playerRigitBody.freezeRotation = true;

		return;
	}

	private void PlayerMove () {
		//float horInput = Input.acceleration.x;
		float horInput = Input.GetAxis ("Horizontal");
		Vector3 movement = Vector3.zero;

		movement.x = horInput * speed;
		movement *= Time.deltaTime;
		player.transform.position += movement;

		player.transform.position = new Vector3 (Mathf.Clamp (player.transform.position.x, Managers.Screen.leftBound - player.transform.localScale.x / 2f,
			Managers.Screen.rightBound + player.transform.localScale.x / 2f),
			player.transform.position.y, player.transform.position.z);

		return;
	}

	public void StopMainAnimation () {
		player.GetComponent <Animation> ().Stop (PLAYER_ROTATION_ANIMATION);

		return;
	}

	public void StartMainAnimation () {
		player.GetComponent <Animation> ().Play (PLAYER_ROTATION_ANIMATION);

		return;
	}

	private void StartTrueScaleAnimation () {
		player.GetComponent <Animation> ().Play (PLAYER_GAME_POSITION);

		return;
	}

	public void StartNewSkinAnimation () {
		if (!player.gameObject.activeSelf)
			SetVisibility (true);

		player.GetComponent<Animation> ().Play (NEW_SKIN_ANIMATION);

		return;
	}

	public void ScoreIncreaze () {
		score++;
		
		return;
	}

	public void SetSkin (Material material) {
		if (material == null) {
			material = Resources.Load (SkinController.SKIN_RESOURCES_PATH + SkinController.DEFAULT_SKIN) as Material;
		}

		currentSkinName = material.name;
		player.GetComponent<Renderer> ().material = material;

		return;
	}

	public void SetIgnoreOneBlock (bool isIgnore) {
		player.GetComponent<BoxCollider> ().isTrigger = isIgnore;
		isIgnoreBlock = isIgnore;

		return;
	}
}
