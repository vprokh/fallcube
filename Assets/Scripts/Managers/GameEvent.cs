﻿
public static class GameEvent {
	public const string SUCCESSFUL_MOVE = "SUCCESSFUL_MOVE";
	public const string FAIL_MOVE = "FAIL_MOVE";
	public const string START_GAME = "START_GAME";
	public const string SPAWN_BLOCK = "SPAWN_BLOCK";
	public const string RESTART_GAME = "RESTART_GAME";
	public const string WIDTH_CHANGE = "WIDTH_CHANGE";
	public const string SPEED_SLOW = "SPEED_SLOW";

	public static bool isMusicOn = true;
	public static bool isSoundOn = true;
	public static bool isRewarding = false;
	public static int loseCounter = 0;
}