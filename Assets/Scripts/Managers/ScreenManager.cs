﻿using System.Collections;
using UnityEngine;

public class ScreenManager : MonoBehaviour {
	public const float SCREEN_OFFSET = 2f;

	public float screenSize { get; private set; }
	public float leftBound { get; private set; }
	public float rightBound { get; private set; }
	public float topBound { get; private set; }
	public float bottomBound { get; private set; }

	void Awake () {
		float dist = Vector3.Distance (transform.position, Camera.main.transform.position);

		leftBound = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x + transform.localScale.x / 2;
		rightBound = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x - transform.localScale.x / 2;
		topBound = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;
		bottomBound = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
		screenSize = Mathf.Abs (leftBound) + Mathf.Abs (rightBound);
	}

}
